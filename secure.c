/**
 * \file secure.c
 * \brief Test for potential error when executing standard stream routines.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-04-14 Tue 05:51 PM
 * \copyright
 * Copyright (C) 2020 sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "secure.h"

FILE * secure_fopen(const char * pathName, const char * mode)
{
	FILE * file;
	if ((file = fopen(pathName, mode)) == NULL) {
		fprintf(stderr, "error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	return file;
}

FILE * secure_fclose(FILE * stream)
{
	if (fclose(stream) == EOF) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	return NULL;
}

void secure_fputc(const int byte, FILE * const stream)
{
	if (fputc(byte, stream) == EOF) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
}

int secure_fgetc(FILE * const stream)
{
	int currChar;
	currChar = fgetc(stream);
	if (ferror(stream) != 0) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	return currChar;
}

size_t secure_fwrite(void * const destination, const size_t atomicSize, size_t cardinal, FILE * const stream)
{
	size_t nWrites;
	if ((nWrites = fwrite(destination, atomicSize, cardinal, stream)) != cardinal) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	return nWrites;
}

size_t secure_fread(void * const destination, const size_t atomicSize, size_t cardinal, FILE * const stream)
{
	size_t nReads;
	if ((nReads = fread(destination, atomicSize, cardinal, stream)) != cardinal) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	return nReads;
}

long secure_ftell(FILE * const stream)
{
	long currPos;
	if ((currPos = ftell(stream)) == -1) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	return currPos;
}

void secure_fseek(FILE * const stream, const long offset, const int whence)
{
	if (fseek(stream, offset, whence) == -1) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
}
