/**
 * \file table.h
 * \brief Contains routine that interacts with the table.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-04-14 Tue 05:51 PM
 * \copyright
 * Copyright (C) 2020 sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef __TABLE__
	#define __TABLE__
	#include <stdio.h> 
	#include <stdlib.h>
	#include <stdbool.h>
	#include <limits.h>
	#include <errno.h>
	#include <string.h>
	#include "secure.h"
	#include "vector.h"

	#define TABLE_SIZE 256

	DEFINE_VECTOR_TYPE(unsigned char, UC)

	/**
	 * \struct StackElement
	 * \brief Contains information about plain and compressed file.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \var FILE * plainFile
	 * Plain file stream.
	 * \var FILE * compressedFile
	 * Compressed file stream.
	 * \var unsigned long long freq[TABLE_SIZE]
	 * List of frequencies of each character.
	 * \var VectorUC * index
	 * List of index of each character appearing at least once in the plain file.
	 * \var unsigned short count
	 * Number of unique character in the plain file.
	 * \var VectorUC * code[TABLE_SIZE]
	 * List of all the huffman codes for each character.
	 */
	typedef struct {
		FILE * plainFile;
		FILE * compressedFile;
		unsigned long long freq[TABLE_SIZE];
		VectorUC * index;
		unsigned short count;
		VectorUC * code[TABLE_SIZE];
	} Table;

	/**
	 * \struct StackElement
	 * \brief Represents a frequency of character group during Huffman's tree generation.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \var unsigned long long frequency
	 * Sum of the individual frequencies of characters stored in UCharacters.
	 * \var VectorUC * UCharacters
	 * List of grouped characters during Huffman's tree generation.
	 */
	typedef struct {
		unsigned long long frequency;
		VectorUC * UCharacters;
	} Frequency;

	DEFINE_VECTOR_TYPE(Frequency *, Freq)

	/**
	 * \fn Table * table_get(void)
	 * \brief Gets static table address.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \return The static table address.
	 */
	Table * table_get(void);

	/**
	 * \fn void table_index_free(Table * const table)
	 * \brief Frees index vector inside given table.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table Targeted table address.
	 */
	void table_index_free(Table * const table);

	/**
	 * \fn type name(param)
	 * \brief Frees huffman code array of vectors inside given table.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table Targeted table address.
	 */
	void table_code_free(Table * const table);

	/**
	 * \fn void table_set_index(Table * const table)
	 * \brief Sets table->index vector with character(s) ordered gradually in function of their frequency.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 january 2020
	 * \param table Targeted table address.
	 */
	void table_set_index(Table * const table);

	/**
	 * \fn Frequency * Frequency_malloc(const size_t UCCapacity)
	 * \brief Allocates frequency structured object and returns its address.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param UCCapacity Capacity of the vector contained inside the frequency.
	 * \return Frequency address.
	 */
	Frequency * Frequency_malloc(const size_t UCCapacity);

	/**
	 * \fn Frequency * Frequency_free(Frequency * const freq)
	 * \brief Frees frequency structured object and returns NULL.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param freq Frequency to be freed.
	 * \return Always NULL in order to avoid freeing same object twice.
	 */
	Frequency * Frequency_free(Frequency * const freq);

	/**
	 * \fn void Frequency_merge(const Frequency * const left, Frequency * const intoRight)
	 * \brief Adds two frequencies and combines their vectors to finally store it inside the right frequency.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param left Left frequency.
	 * \param intoRight Right frequency.
	 */
	void Frequency_merge(const Frequency * const left, Frequency * const intoRight);

	/**
	 * \fn VectorFreq * VectorFreq_allocinit(const Table * const table)
	 * \brief Allocates and initializes a vector of frequency objects based on current frequency array of given table.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table Source table address.
	 * \return Vector composed of frequency objects.
	 */
	VectorFreq * VectorFreq_allocinit(const Table * const table);

	/**
	 * \fn VectorFreq * VectorFreq_free(VectorFreq * const vector)
	 * \brief Frees vector of frequency objects and returns NULL.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param vector Vector of frequencies to be freed.
	 * \return Always NULL in order to avoid freeing same object twice.
	 */
	VectorFreq * VectorFreq_free(VectorFreq * const vector);

	/**
	 * \fn size_t VectorFreq_get_ascending_index(VectorFreq * const frequencies, const size_t startIndex, const Frequency * const untidyFreq)
	 * \brief Figures out the next ascending index position of untidyFreq frequency in frequencies vector.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param Frequencies Vector of frequencies.
	 * \param startIndex Current's position index.
	 * \param untidyFreq Frequency to place orderly.
	 * \return New ascending index position inside frequencies for untidyFreq.
	 */
	size_t VectorFreq_get_ascending_index(VectorFreq * const frequencies, const size_t startIndex, const Frequency * const untidyFreq);

	/**
	 * \fn void table_malloc_codespace(Table * const table);
	 * \brief Allocates huffman code's vectors inside given table.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param Targeted table address.
	 */
	void table_malloc_codespace(Table * const table);

	/**
	 * \fn void table_cat_code(Table * const table, VectorUC * UCharacters, unsigned char bitCode)
	 * \brief Adds bitCode character at the beginning of all concerned UCharacter's huffman code inside given table.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table Targeted table address.
	 * \param UCharacters Targeted characters.
	 * \param bitCode New huffman bit character ('1' or '0').
	 */
	void table_cat_code(Table * const table, VectorUC * UCharacters, unsigned char bitCode);

	/**
	 * \fn void table_set_code(Table * const table);
	 * \brief Sets all huffman's code for each ASCII character inside given table.
	 * \author sangsoic <sangsoic@protonmail.com>
	 * \version 0.1
	 * \date 23 January 2020
	 * \param table Targeted table address.
	 */
	void table_set_code(Table * const table);
#endif
