/**
 * \file table.c
 * \brief Contains routine that interacts with the table.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-04-14 Tue 05:51 PM
 * \copyright
 * Copyright (C) 2020 sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "table.h"

DEFINE_VECTOR_WRAPPER_MALLOC(unsigned char, UC)
DEFINE_VECTOR_WRAPPER_RSHIFT(unsigned char, UC)

DEFINE_VECTOR_WRAPPER_MALLOC(Frequency *, Freq)
DEFINE_VECTOR_WRAPPER_RSHIFT(Frequency *, Freq)
DEFINE_VECTOR_WRAPPER_LSHIFT(Frequency *, Freq)

Table * table_get(void)
{
	static Table table;
	return &table;
}

void table_index_free(Table * const table)
{
	table->count = 0;
	table->index = Vector_free(table->index);
}

void table_code_free(Table * const table)
{
	size_t i;
	for (i = 0; i < table->count; i++) {
		table->code[i] = Vector_free(table->code[i]);
	}
}

void table_set_index(Table * const table)
{
	size_t i, j;
	VectorUC * index;
	index = VectorUC_malloc(table->count);
	for (i = 0; i < TABLE_SIZE; i++) {
		if (table->freq[i] != 0) {
			j = 0;
			while ((j < index->cardinal) && (table->freq[i] > table->freq[index->value[j]])) {
				j++;
			}
			if (j < index->cardinal) {
				VectorUC_rshift(index->value+j, index->cardinal-j+1);
			}
			index->value[j] = i;
			index->cardinal++;
		}
	}
	table->index = index;
}

Frequency * Frequency_malloc(const size_t UCCapacity)
{
	Frequency * freq;
	if (UCCapacity == 0) {
		fprintf(stderr,"error : cannot allocate 0 sized objects.\n");
		exit(EXIT_FAILURE);
	}
	freq = malloc(sizeof(Frequency));
	if (freq == NULL) {
		fprintf(stderr,"error : %s.\n", strerror(errno));
		exit(EXIT_FAILURE);
	}
	freq->frequency = 0;
	freq->UCharacters = VectorUC_malloc(UCCapacity);
	return freq;
}

Frequency * Frequency_free(Frequency * const freq)
{
	freq->UCharacters = Vector_free(freq->UCharacters);
	free(freq);
	return NULL;
}

void Frequency_merge(const Frequency * const left, Frequency * const intoRight)
{
	size_t i;
	intoRight->frequency += left->frequency;
	for (i = 0; i < left->UCharacters->cardinal; i++) {
		VectorUC_rshift(intoRight->UCharacters->value,intoRight->UCharacters->cardinal+1);
		intoRight->UCharacters->value[0] = left->UCharacters->value[i];
		intoRight->UCharacters->cardinal++;
	}
}

VectorFreq * VectorFreq_allocinit(const Table * const table)
{
	VectorFreq * vector;
	size_t i;
	vector = VectorFreq_malloc(table->count);
	for (i = 0; i < table->count; i++) {
		vector->value[i] = Frequency_malloc(table->count);
		vector->value[i]->frequency = table->freq[table->index->value[i]];
		vector->value[i]->UCharacters->value[0] = table->index->value[i];
		vector->value[i]->UCharacters->cardinal = 1;
	}
	vector->cardinal = table->count;
	return vector;
}

VectorFreq * VectorFreq_free(VectorFreq * const vector)
{
	size_t i;
	for (i = 0; i < vector->cardinal; i++) {
		vector->value[i] = Frequency_free(vector->value[i]);
	}
	return Vector_free(vector);
}

size_t VectorFreq_get_ascending_index(VectorFreq * const frequencies, const size_t startIndex, const Frequency * const untidyFreq)
{
	size_t i;
	i = startIndex;
	while ((i < frequencies->cardinal) && (untidyFreq->frequency > frequencies->value[i]->frequency)) {
		i++;
	}
	if (i < frequencies->cardinal) {
		VectorFreq_rshift(frequencies->value+i, frequencies->cardinal-i);
		i++;
	}
	return i;
}

void table_malloc_codespace(Table * const table)
{
	size_t i;
	for (i = 0; i < table->count; i++) {
		table->code[table->index->value[i]] = VectorUC_malloc(table->count-1);
	}
}

void table_cat_code(Table * const table, VectorUC * UCharacters, unsigned char bitCode)
{
	VectorUC * currCode;
	size_t i;
	for (i = 0; i < UCharacters->cardinal; i++) {
		currCode = table->code[UCharacters->value[i]];
		VectorUC_rshift(currCode->value,currCode->cardinal+1);
		currCode->value[0] = bitCode;
		currCode->cardinal++;
	}
}

void table_set_code(Table * const table)
{
	VectorFreq * frequencies;
	Frequency  * leftFreq, * rightFreq, * savedFreq;
	size_t i, j;
	frequencies = VectorFreq_allocinit(table);
	table_malloc_codespace(table);
	for (i = 0; i < frequencies->cardinal-1; i++) {
		leftFreq = frequencies->value[i];
		rightFreq = frequencies->value[i+1];
		table_cat_code(table,leftFreq->UCharacters,'0');
		table_cat_code(table,rightFreq->UCharacters,'1');
		Frequency_merge(leftFreq,rightFreq);
		savedFreq = rightFreq;
		VectorFreq_lshift(frequencies->value+i+1,frequencies->cardinal-(i+1));
		j = VectorFreq_get_ascending_index(frequencies,i+1,savedFreq);
		frequencies->value[j-1] = savedFreq;
	}
	frequencies = VectorFreq_free(frequencies);
}
