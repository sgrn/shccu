/**
 * \file option.c
 * \brief Contains routines which interface with the user.
 * \author sangsoic <sangsoic@protonmail.com>
 * \version 0.1
 * \date 2020-04-14 Tue 05:51 PM
 * \copyright
 * Copyright (C) 2020 sangsoic
 *
 * This program is free software: you can redistribute it and/or modify it under the terms of the 
 * GNU General Public License as published by the Free Software Foundation, either version 3 of the 
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License along with this program.
 * If not, see <https://www.gnu.org/licenses/>.
 */
#include "option.h"

void option_help(void)
{
	printf("usage : huffman ( -(c|d) SOURCE DESTINATION ) | -h\n");
	printf("\t-c\tcompress using string format. (increase file size)\n");
	printf("\t-b\tcompress using binary format. (decrease file size)\n");
	printf("\t-d\tdecompress file. (automatically detects compression type)\n");
	printf("\t-h\tprint help menu.\n");
	printf("exit status :\n");
	printf("\t0\tsuccess.\n");
	printf("\telse\terror.\n");
}

int option_choice(int argc, char ** argv)
{
	int EXIT_STATUS;
	EXIT_STATUS = 2;
	if (strcmp(argv[1], "-c") == 0) {
		if (argc == 4) {
			act(argv[2],argv[3], COMPRESS);
			EXIT_STATUS = EXIT_SUCCESS;
		}
	} else if (strcmp(argv[1], "-d") == 0) {
		if (argc == 4) {
			act(argv[3],argv[2], DECOMPRESS);
			EXIT_STATUS = EXIT_SUCCESS;
		}
	} else if (strcmp(argv[1], "-b") == 0) {
		if (argc == 4) {
			act(argv[2],argv[3], BINCOMPRESS);
			EXIT_STATUS = EXIT_SUCCESS;
		}
	} else if (strcmp(argv[1], "-h") == 0){
		option_help();
		EXIT_STATUS = EXIT_SUCCESS;
	} else {
		EXIT_STATUS = 3;
	}
	return EXIT_STATUS;
}


void option_feedback(int status)
{
	switch (status) {
		case 2:
			fprintf(stderr, "error : [de]compression requires SOURCE and DESTINATION.\n");
			option_help();
			break;
		case 3:
			fprintf(stderr, "bad usage : option is not valid.\n");
			option_help();
			break;
		case 4:
			fprintf(stderr, "bad usage : no option passed.\n");
			option_help();
			break;
		default:
			break;
	}
}

int run(int argc, char ** argv)
{
	int EXIT_STATUS;
	if (argc >= 2) {
		EXIT_STATUS = option_choice(argc,argv);
	} else {
		EXIT_STATUS = 4;
	}
	option_feedback(EXIT_STATUS);
	return EXIT_STATUS;
}
